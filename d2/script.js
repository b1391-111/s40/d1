// console.log("HELLO")

fetch("https://jsonplaceholder.typicode.com/posts")
    .then(res => res.json())
    .then(data => console.log(data));

fetch("https://jsonplaceholder.typicode.com/posts/1")
    .then(res => res.json())
    .then(data => console.log(data));

// POST method
fetch("https://jsonplaceholder.typicode.com/posts", {
    method: "POST",
    headers: {
        'Content-Type': 'application/json'
    },
    body: JSON.stringify({
        title: "New Post",
        body: "Hello World",
        userId: 2
    })
})
    .then(res => res.json())
    .then(data => console.log(data));


fetch("https://jsonplaceholder.typicode.com/posts/1", {
    method: "PUT",
    headers: {
        'Content-Type': 'application/json'
    },
    body: JSON.stringify({
        id: 1,
        title: "Updated Post",
        body: "Hello Again",
        userId: 1
    })
})
    .then(res => res.json())
    .then(data => console.log(data));


fetch("https://jsonplaceholder.typicode.com/posts/1", { method: "DELETE" })
    .then(res => res.json())
    .then(data => console.log(data));